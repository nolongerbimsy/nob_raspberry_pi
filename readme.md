# Nob raspberry pi setup

## Background
This hardware setup was made to measure "external factors" within the bedrooms of participants of a 
[quantified self study](https://bitbucket.org/nolongerbimsy/), the study was about sleep inertia and these factors measured to find out if they would affect a participants sleep.

Raspberry Pi's in combination with BME280 
([VMA335](https://www.velleman.eu/downloads/29/vma335_a4v01.pdf)) and 
[TSL2591](https://cdn-learn.adafruit.com/downloads/pdf/adafruit-tsl2591.pdf) 
sensors were used to measure: air pressure (hPA), relative humidity (%), lux (lx) and temperature (ºC).

## Requirements:

#### Hardware:
 - Raspberry Pi [3b+](https://static.raspberrypi.org/files/product-briefs/Raspberry-Pi-Model-Bplus-Product-Brief.pdf) or [4b](https://static.raspberrypi.org/files/product-briefs/Raspberry-Pi-4-Product-Brief.pdf)
 - [VMA335](https://www.velleman.eu/downloads/29/vma335_a4v01.pdf)
 - [TSL2591](https://cdn-learn.adafruit.com/downloads/pdf/adafruit-tsl2591.pdf)
 


#### Software:
- [Python 3](https://www.python.org/)
- [bme280 (0.2.3)](https://github.com/rm-hull/bme280)
- [smbus2 (0.3.0)](https://github.com/kplindegaard/smbus2)
- [adafruit_tsl2591 (1.1.4)](https://github.com/adafruit/Adafruit_CircuitPython_TSL2591)
- busio (Will be installed with pip3 as dependency of adafruit_tsl2591.)
- board (Will be installed with pip3 as dependency of adafruit_tsl2591.)

To install all the required Python 3 packages at once:  

    pip3 install bme280 smbus2 adafruit_tsl2591

## Schematic
![Schematic for sensors on raspberry_pi.](Images/bme280_tsl2591_raspberry_pi_schematic.png)

## Code logic

The main script running at all times is the myscript.sh script, which runs the sensor_test.py which collects all the data. This data is stored in a file with the first initial (x in the code) and then the date of the recording. 

Cronjob is used to verify if this script is running once every minute by running the newscript.sh. This ensures minimal loss of data. At one before midnight the killswitch.sh program kills the current running program by checking its name in the processes.

Then the Postscript.sh is run which first runs the PythonPush.py, this uses requests to push the data to the pilogger upload and after that stores the file in a new folder called Pushed data. This creates an easy overview of files that have been succesfully pushed.



## Disclaimer

The whole setup should be interchangeable between different single board computers, however it has only been tested on the Raspberry Pi models [3b+](https://static.raspberrypi.org/files/product-briefs/Raspberry-Pi-Model-Bplus-Product-Brief.pdf) and [4b](https://static.raspberrypi.org/files/product-briefs/Raspberry-Pi-4-Product-Brief.pdf).

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.