#!/usr/bin/env python3

import requests
import sys
import os.path
filepath = sys.argv[1]
filename = os.path.basename(filepath)
print("'", filename, "'", sep="")
# Url to upload it to, change this to whatever works for you.
url = 'https://user_site.com/pilogger/logger/upload'
multiple_files = [
	('files', (filename, open(filepath, 'rb')))]
r = requests.post(url, files=multiple_files)
print(r.text, "\n")
print(r.headers)
