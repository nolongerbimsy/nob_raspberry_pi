#!/bin/bash
saved_date=$(date | tr " " "\t" | cut -f1-4 | tr "\t" "_")
export PYTHONPATH=/home/pi/.local/lib/python3.7
nohup python3 /home/pi/sensor_test.py >> /home/pi/X_${saved_date}_sensor_output.csv &
