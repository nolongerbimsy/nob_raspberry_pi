#!/bin/bash

saved_date=$(date | tr " " "\t" | cut -f1-4 | tr "\t" "_")
echo $saved_date
python3 /home/pi/PythonPush.py /home/pi/X_${saved_date}_sensor_output.csv
mv /home/pi/X_${saved_date}_sensor_output.csv /home/pi/PushedData
