#!/usr/bin/env python3
import board
import bme280
import smbus2
import busio
import adafruit_tsl2591
from time import sleep
from datetime import datetime

# Initialize Adafruit TSL2591 light Sensor.
i2c = busio.I2C(board.SCL, board.SDA)
sensor = adafruit_tsl2591.TSL2591(i2c)

# Initialize BME280 sensor which measures: Air Pressure, Humidity, Temperature.
port = 1
address = 0x76  # I2C address of the used sensor.
bus = smbus2.SMBus(port)

bme280.load_calibration_params(bus, address)

# Keep the output running and produce a csv file.
while True:
    bme280_data = bme280.sample(bus, address)
    now = datetime.now()
    timestamp = datetime.timestamp(now)
    dt_object = datetime.fromtimestamp(timestamp)
    # pressure, humidity, temperature, lux,
    print(dt_object, ",", bme280_data.pressure, ",", bme280_data.humidity, ",", bme280_data.temperature, ",",
          sensor.lux, ",", sensor.visible, ",", sensor.infrared, ",", sensor.full_spectrum, ",", sensor.raw_luminosity,
          sep="")
    sleep(1)
